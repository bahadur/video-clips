<?php
/**
 * Video file metadata
 *
 * PHP version 5
 *
 * @author     Bahadur O. <bahadur.o@allshoreresources.com>
 * @copyright  2016 Allshoreresources
 * @version   0.1
 */

require 'MyFFMpeg.php';
$msg = "Please select a video to upload.";
$err = false;
if(isset($_POST['submit'])) {

    /** @var  $file_type */
    $file_type          = $_FILES['video_file']['type'];

    /** @var $file_size */
    $file_size          = $_FILES['video_file']['size'];

    /** @var $file_ext */
    $file_ext           = end(explode(".",$_FILES['video_file']['name']));

    /** @var $allowed_extensions */
    $allowed_extensions = array("mp4");

    /** @var $pattern */
    $pattern            = implode($allowed_extensions,"|");

    /** @var $clips */
    $clips              = $_POST['clips'];

    /** @var  timestamp $time  */
    $time               = time();

    /** @var $file_name */
    $file_name          = "input_file_$time.".$file_ext;

    /** @var  $dir */
    $dir                = "videos_$time/";

    if($_FILES['video_file']['size'] > 0 ){ // If file is not provided

        if(preg_match("/({$pattern})$/i", $file_name) ){ // If provided file is not video file.

            if($_FILES['video_file']['error'] > 0){ // If there is any error on uploading

                $msg =  "Unexpected error occured, please try again.";
                $err = true;
            } else {

                mkdir(__DIR__.'/'.$dir);
                $base_dir = __DIR__.'/'.$dir;
                /** Moving uploaded video file */
                if(!move_uploaded_file($_FILES['video_file']['tmp_name'], $base_dir.$file_name)){
                    $msg = "Something went wrong moving video file.";
                    $err = true;
                } else {

                    $ffmpegOptions = array(
                        'input_file' => $base_dir.$file_name,
                        'clips' => array()
                    );

                    // Input array for Video clips
                    $videoClips = array(
                        array(
                            "start" => '00:00:00',
                            "end"  =>  '00:01:00'
                        ),
                        array(
                            "start" => '00:01:00',
                            "end"  =>  '00:04:00'
                        ),
                        array(
                            "start" => '0:04:00',
                            "end"  =>  '00:05:00'
                        )
                    );

                    /** Preparing array for clips  */
                    $clip = 1;
                    foreach($videoClips as $node ){
                        $ffmpegOptions['clips'][] = array(
                            'format'        => 'mp4',
                            'start_time'    => $node['start'],
                            'duration'      => $node['end'],
                            'output_file'   => 'clip'.$clip,
                            'dir'           => $base_dir
                        );
                        $clip++;
                    }

                    /** @Objectg $ffmpeg; Instance of MyFFMpeg */
                    $ffmpeg = new MyFFMpeg();
                    $ffmpeg->input($ffmpegOptions);

                    /** Execute ffmpeg command */
                    $ffmpeg->execute();

                }
            }
        } else {

            $msg = "Extension not allowd";
            $err = true;
        }
    } else {

        $msg = "Please select a video files.";
        $err = true;
    }
} else {
    $msg = "Please select video.";
    $err = true;
}
?>