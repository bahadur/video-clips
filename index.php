<?php require "settings.php";?>
<!doctype html>
<html>
    <meta charset="utf-8">
    <head>
        <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
        <title>Generate video clips</title>
    </head>
    <body>

        <?php if($err) : ?>

            <div style="color: red;"><?php echo $msg?></div>
            <a href="index.php" >Back..</a>
        <?php  else :  ?>

            <h2>Select video file to upload </h2>
            <h5>Format allowed: MP4</h5>
            <form action="video_upload.php" method="post" enctype="multipart/form-data">

                Select Video: <input type="file" name="video_file" id="video_file"> </input><br>
                <input type="submit" name="submit" value="Generate Clips" id="btn_submit"><br />
            </form>

            <script language="javascript1.2">

                $(document).ready(function() {

                    /** Run after select video file. **/
                    $("#video_file").change(function (e) {
                        /** video file extension to be upload **/
                        var fileExtension = ['mp4'];
                        /** if is not video **/
                        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {

                            alert("Only format allowed is : " + fileExtension.join(', '));
                            $(this).replaceWith($(this).val('').clone(true));
                        } else if ((this.files[0].size / 1048576) > <?php echo $upload_max_filesize?> ) {
                            /** check maximum file size allowed **/

                            alert("File size is too big. Maximum file size allowd: <?php echo $upload_max_filesize?>MB");
                            $(this).replaceWith($(this).val('').clone(true));
                        }
                    });
                });

            </script>
        <?php endif; ?>
    </body>
</html>
